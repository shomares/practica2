using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[Route("api/[controller]")]
[ApiController]
[Authorize]
public class ProductoController : ControllerBase
{
    private readonly IDaoProducto daoProducto;

    public ProductoController(IDaoProducto daoProducto)
    {
        this.daoProducto = daoProducto;
    }

    [HttpGet]
    public async Task<IActionResult> GetProductos ()
    {
        var r = await this.daoProducto.GetProductos();

        if(r !=null)
        {
            return this.Ok(r);
        }

        return this.NotFound();
    }

    [HttpPost]
    public async void Save([FromBody]Producto producto)
    {
        await this.daoProducto.Register(producto);
    }

}