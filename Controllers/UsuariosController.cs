using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace examenAxity1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
            
        private readonly IUsuarioService usuarioService;

        public UsuariosController(IUsuarioService usuarioService)
        {
            this.usuarioService = usuarioService;
        }

        [HttpPost]
        public async Task<IActionResult> Authorize ([FromBody]Usuario usuario)
        {
            var result = await this.usuarioService.Authenticate(usuario.Id, usuario.Password);
            if(result != null)
            {
                return this.Ok(result);
            }

            return this.NotFound();
        }

        [HttpPut]
        public async void Save([FromBody]Usuario usuario)
        {
            await this.usuarioService.Register(usuario);
        }




    }
}