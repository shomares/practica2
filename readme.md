# Project Title

Practica sencilla de aspnet core

## Getting Started

1. Correr el archivo Src/Database/script1.sql
2. Ejecutar dotnet run
3. Abrir en el navegador https://localhost:5001/index.html
4. Iniciar con las credenciales: user->test password->1234567

### Prerequisites

Es necesario:
1. dotnetcore
2. npm
3. Sql Server


