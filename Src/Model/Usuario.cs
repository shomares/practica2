using System.ComponentModel.DataAnnotations.Schema;

[Table("Users")]
public class Usuario
{
    public string Id { get;  set; }
    public string Password { get;  set; }
    public string Token { get;  set; }
}