using System.ComponentModel.DataAnnotations.Schema;

[Table("Products")]
public class Producto
{
    public int Id {get; set;}

    public string Nombre {get; set;}

    public string Marca {get; set;}

    public double? Precio {get; set;}
    
}