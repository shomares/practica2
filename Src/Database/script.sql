CREATE DATABASE Productos;
GO
IF exists(SELECT name from sys.tables where name = 'Products')
    DROP TABLE Products
GO
CREATE TABLE Products(
        Id int Identity PRIMARY KEY,

        Nombre varchar(200),

        Marca varchar(200),

        Precio float
)

IF exists (SELECT name from sys.tables where name = 'Users')
    DROP TABLE Users
go
CREATE TABLE Users(
        Id varchar(200) PRIMARY KEY,
        [Password] varchar(200), 
        Token varchar(200)
)

--c34a165075f5

INSERT INTO Users (Id, [Password]) values ('test', 'ByJufuxxpEN/0vA5Pz8uqUEBn83gdQqEU3AeG8GbxVI=')
INSERT INTO Products (Nombre, Marca, Precio) values ('Prueba1', 'Marca1', 121)
INSERT INTO Products (Nombre, Marca, Precio) values ('Prueba2', 'Marca2', 122)
INSERT INTO Products (Nombre, Marca, Precio) values ('Prueba3', 'Marca3', 123)
INSERT INTO Products (Nombre, Marca, Precio) values ('Prueba4', 'Marca4', 124)
INSERT INTO Products (Nombre, Marca, Precio) values ('Prueba5', 'Marca5', 125)
INSERT INTO Products (Nombre, Marca, Precio) values ('Prueba6', 'Marca6', 126)

