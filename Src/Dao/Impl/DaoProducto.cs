using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

public class DaoProducto : IDaoProducto
{

    private readonly DbProductoContext dbProductoContext;

    public DaoProducto(DbProductoContext dbProductoContext)
    {
        this.dbProductoContext = dbProductoContext;
    }
    public async Task<IEnumerable<Producto>> GetProductos()
    {
        return await this.dbProductoContext.Productos.ToArrayAsync();
    }

    public async Task Register(Producto producto)
    {
        await this.dbProductoContext.Productos.AddAsync(producto);
        this.dbProductoContext.SaveChanges();
    }

    public void Dispose()
    {
        this.dbProductoContext.Dispose();
    }
}