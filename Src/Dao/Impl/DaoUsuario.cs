using System.Threading.Tasks;

public class DaoUsuario : IDaoUsuario
{
    private readonly DbProductoContext dbProductoContext;

    public DaoUsuario(DbProductoContext dbProductoContext)
    {
        this.dbProductoContext = dbProductoContext;
    }

    public async Task<bool> Authenticate(string user, string password)
    {
        var userDb = await this.GetUser(user);
        return userDb != null && userDb.Password == password;
    }

    public void Dispose()
    {
        this.dbProductoContext.Dispose();
    }

    public async Task<Usuario> GetUser(string user)
    {
        return await this.dbProductoContext.Usuarios.FindAsync(user);
    }

    public async Task Register(Usuario usuario)
    {
        await this.dbProductoContext.Usuarios.AddAsync(usuario);
        this.dbProductoContext.SaveChanges();
    }
}