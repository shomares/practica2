using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public interface IDaoProducto : IDisposable
{
    Task<IEnumerable<Producto>> GetProductos();

    Task Register (Producto producto);
}