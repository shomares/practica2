using System;
using System.Threading.Tasks;

public interface IDaoUsuario:  IDisposable
{

    Task<bool> Authenticate (string user, string password);

    Task Register (Usuario usuario);
    Task<Usuario> GetUser(string user);
}