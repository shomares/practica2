using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;


public class UsuarioService : IUsuarioService
{
    private readonly IDaoUsuario daoUsuario;
    private readonly IHash hash;

     private readonly AppSettings appSettings;

    public UsuarioService(IDaoUsuario daoUsuario, IHash hash, IOptions<AppSettings> appSettings)
    {
        this.daoUsuario = daoUsuario;
        this.hash = hash;
        this.appSettings = appSettings.Value;
    }

    public async Task<Usuario> Authenticate(string user, string password)
    {
        string pass = this.hash.GetHash(password);
        if(await this.daoUsuario.Authenticate(user, pass))
        {
            Usuario usuarioDb = await this.daoUsuario.GetUser(user);
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, usuarioDb.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            usuarioDb.Token = tokenHandler.WriteToken(token);

            usuarioDb.Password = null;

            return usuarioDb;
        }

        return null;
    }

    public async Task Register(Usuario usuario)
    {
        var password = this.hash.GetHash(usuario.Password);

        await this.daoUsuario.Register(new Usuario{

            Password = password,
            Id =  usuario.Id
        });

    }
}