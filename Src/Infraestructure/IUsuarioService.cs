using System.Threading.Tasks;

public interface IUsuarioService
{
    Task<Usuario> Authenticate(string user, string password);

    Task Register (Usuario usuario);
    
}