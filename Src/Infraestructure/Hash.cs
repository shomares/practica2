using System;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Options;

public class Hash : IHash
{

    private readonly AppSettings appSettings;

    public Hash(IOptions<AppSettings> appSettings)
    {
        this.appSettings = appSettings.Value;
    }

    public string GetHash(string cadena)
    {
         var valueBytes = KeyDerivation.Pbkdf2(  
						 password: cadena,  
						 salt: Encoding.UTF8.GetBytes(this.appSettings.Hash),  
						 prf: KeyDerivationPrf.HMACSHA512,  
						 iterationCount: 10000,  
						 numBytesRequested: 256 / 8);  

	 return Convert.ToBase64String(valueBytes);  
    }
}